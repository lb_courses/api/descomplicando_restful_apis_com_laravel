<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\MasterApiController;
use App\Models\Cliente;


class ClienteApiController extends MasterApiController{

    protected $model;
    protected $path = 'clientes';
    protected $upload = 'image';
    protected $width = 177 ;
    protected $heigth = 236 ;
    protected $totalPage = 20 ;

    public function __construct(Cliente $clientes , Request $request) {
         $this->model = $clientes;
         $this->request   = $request;
        }


        public function index() {         
            $data = $this->model->paginate( $this->totalPage);           
            return response()->json($data);
        }

        public function documento($id)
        {
    
            if(  !$data = $this->model->with('documento')->find($id) ) {
                return response()->json(["error" => "Nada foi encontrado!" ] , 404);
            } else {
                return response()->json($data  , 201);
            }  
            
        }

        public function telefone($id)
        {
    
            if(  !$data = $this->model->with('telefone')->find($id) ) {
                return response()->json(["error" => "Nada foi encontrado!" ] , 404);
            } else {
                return response()->json($data  , 201);
            }  
            
        }


        public function alugados($id)
        {
    
            if(  !$data = $this->model->with('filmesAlugados')->find($id) ) {
                return response()->json(["error" => "Nada foi encontrado!" ] , 404);
            } else {
                return response()->json($data  , 201);
            }  
            
        }
        
        
        

}



// class ClienteApiController extends Controller
// {

//     public function __construct(Cliente $cliente , Request $request)
//     {
//      $this->cliente = $cliente;
//      $this->request   = $request;
//     }

//     /**
//      * Display a listing of the resource.
//      *
//      * @return \Illuminate\Http\Response
//      */
//     public function index() {
//         $data = $this->cliente->all();
//         // dd($data);

//         return response()->json($data);
//     }

    

//     /**
//      * Store a newly created resource in storage.
//      *
//      * @param  \Illuminate\Http\Request  $request
//      * @return \Illuminate\Http\Response
//      */
//     public function store(Request $request)
//     {
//         $this->validate($request , $this->cliente->rules());

//         $dataForm = $request->all();

//         if( $request->hasFile('image') && $request->file('image')->isValid() ){
//             $extension = $request->image->extension();

//             $name = uniqid(date('His'));
//             $nameFile = "{$name}.{$extension}";
//             Image::configure(array('driver' => 'imagick'));
            
//             $upload = Image::make($dataForm['image'])->resize( 177, 236 )->save(storage_path("app/public/clientes/$nameFile" , 70));
//             // $upload = Image::make("public/{$nameFile}")->resize(300, 200);

//             // return response()->json("ATE AQUI ok"  , 201);
            
//             if( !$upload ){
//                 return response()->json(["error" => "falha ao fazer o upload"] , 500);
//             } else {
//                 $dataForm['image'] = $nameFile;
//             }


//         }

//         $data = $this->cliente->create($dataForm);


//         return response()->json($data  , 201);

//     }

//     /**
//      * Display the specified resource.
//      *
//      * @param  int  $id
//      * @return \Illuminate\Http\Response
//      */
//     public function show($id)
//     {

//         if(  !$data = $this->cliente->find($id) ) {
//             return response()->json(["error" => "Nada foi encontrado!" ] , 404);
//         }

//         $data = $this->cliente->find($id);
//         return response()->json($data  , 201);
//     }

//     /**
//      * Update the specified resource in storage.
//      *
//      * @param  \Illuminate\Http\Request  $request
//      * @param  int  $id
//      * @return \Illuminate\Http\Response
//      */
//     public function update(Request $request, $id)
//     {
    
//         if( !$data = $this->cliente->find($id) ) 
//         return response()->json(["error" => "Nada foi encontrado!" ] , 404);   


//         $this->validate($request , $this->cliente->rules());

//         $dataForm = $request->all();

//         if( $request->hasFile('image') && $request->file('image')->isValid() ){

//             if( $data->image ) {
//                 Storage::disk('public')->delete("/clientes/$data->image");
//             }    
            
//             $extension = $request->image->extension();

//             $name = uniqid(date('His'));
//             $nameFile = "{$name}.{$extension}";
//             Image::configure(array('driver' => 'imagick'));
            
//             $upload = Image::make($dataForm['image'])->resize( 177, 236 )->save(storage_path("app/public/clientes/$nameFile" , 70));
//             // $upload = Image::make("public/{$nameFile}")->resize(300, 200);

//             // return response()->json("ATE AQUI ok"  , 201);
            
//             if( !$upload ){
//                 return response()->json(["error" => "falha ao fazer o upload"] , 500);
//             } else {
//                 $dataForm['image'] = $nameFile;
//             }


//         }

//         $data->update($dataForm);


//         return response()->json($data);

//     }

//     /**
//      * Remove the specified resource from storage.
//      *
//      * @param  int  $id
//      * @return \Illuminate\Http\Response
//      */
//     public function destroy($id)
//     {
//         if( !$data = $this->cliente->find($id) ) 
//             return response()->json(["error" => "Nada foi encontrado!" ] , 404);        

//         if( $data->image ) {
//             Storage::disk('public')->delete("/clientes/$data->image");
//         }             

//         $data->delete();
//         return response()->json( ["success" => "Deletado com sucesso!"] , 201);   

//     }
// }
