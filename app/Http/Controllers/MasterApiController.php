<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;


use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

class MasterApiController extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
 
 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // $data = $this->model->all();
        // $data = $this->model->paginate(2);
        $data = $this->model->paginate(10);
        // dd($data);

        return response()->json($data);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request , $this->model->rules());

        $dataForm = $request->all();

        if( $request->hasFile($this->upload) && $request->file($this->upload)->isValid() ){
            
            // $extension = $request->image->extension();
            $extension = $request->file($this->upload)->extension();

            

            $name = uniqid(date('His'));
            $nameFile = "{$name}.{$extension}";
            Image::configure(array('driver' => 'imagick'));
            
            // $upload = Image::make($dataForm[$this->upload])->resize( 177, 236 )->save(storage_path("app/public/{$this->path}/{$nameFile}" , 70));
            $upload = Image::make($dataForm[$this->upload])->resize( $this->width, $this->heigth )->save(storage_path("app/public/{$this->path}/{$nameFile}" , 70));
            // $upload = Image::make("public/{$nameFile}")->resize(300, 200);

            // return response()->json("ATE AQUI ok"  , 201);
            
            if( !$upload ){
                return response()->json(["error" => "falha ao fazer o upload"] , 500);
            } else {
                $dataForm[$this->upload] = $nameFile;
            }


        }

        $data = $this->model->create($dataForm);


        return response()->json($data  , 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if(  !$data = $this->model->find($id) ) {
            return response()->json(["error" => "Nada foi encontrado!" ] , 404);
        }

        $data = $this->model->find($id);
        return response()->json($data  , 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        if( !$data = $this->model->find($id) ) 
        return response()->json(["error" => "Nada foi encontrado!" ] , 404);   


        $this->validate($request , $this->model->rules());

        $dataForm = $request->all();

        if( $request->hasFile('image') && $request->file('image')->isValid() ){

            $arquivo = $this->model->arquivo($id);

            // if( $data->image ) {
            if( $arquivo ) {    
                Storage::disk('public')->delete("/{$this->path}/$arquivo");
            }    
            
            // $extension = $request->image->extension();
            $extension = $request->file($this->upload)->extension();

            $name = uniqid(date('His'));
            $nameFile = "{$name}.{$extension}";
            Image::configure(array('driver' => 'imagick'));
            
            // $upload = Image::make($dataForm['image'])->resize( 177, 236 )->save(storage_path("app/public/{$this->path}/{$nameFile}" , 70));
            $upload = Image::make($dataForm['image'])->resize( $this->width, $this->heigth  )->save(storage_path("app/public/{$this->path}/{$nameFile}" , 70));
            // $upload = Image::make("public/{$nameFile}")->resize(300, 200);

            // return response()->json("ATE AQUI ok"  , 201);
            
            if( !$upload ){
                return response()->json(["error" => "falha ao fazer o upload"] , 500);
            } else {
                $dataForm['image'] = $nameFile;
            }


        }

        $data->update($dataForm);


        return response()->json($data);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if( !$data = $this->model->find($id) ) 
        if( $data = $this->model->find($id) ) {


            if(  method_exists( $this->model, 'arquivo' ) ) {
                Storage::disk('public')->delete("/{$this->path}/{$this->model->arquivo($id)}");
            }             

            $data->delete();
            return response()->json( ["success" => "Deletado com sucesso!"] , 201);   

        } else {
            return response()->json(["error" => "Nada foi encontrado!" ] , 404);        
        }


            

        

    }
}
