<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Documento;
use App\Models\Telefone;
use App\Models\Filme;

class Cliente extends Model
{
    protected $fillable = [
        'name',
        'image',
    ];
    // 'cpf_cnpj'

    public function rules() {

        return [
            'name' => 'required',
            'image' => 'image' ,            
        ];
        // 'cpf_cnpj' => 'required|unique:clientes'
    }

    // vai ser usado no controller para saber o nome do campo image, pois poderia ser avatar
    // ou qualquer outro nome
    public function arquivo($id) {
        $data = $this->find($id);
        return $data->image;
    }

    // faz relacionamento 1 pra 1    
    public function documento(){
        return $this->hasOne(Documento::class, 'cliente_id' , 'id');
    }


    // faz relacionamento 1 pra n
    public function telefone(){
        return $this->hasMany(Telefone::class, 'cliente_id' , 'id');
    }

    // muitos para muitos
    public function filmesAlugados(){
        return $this->belongsToMany(Filme::class , 'locacaos');
    }

}
