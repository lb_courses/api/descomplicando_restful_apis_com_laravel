<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Filme extends Model
{
    protected $fillable = [
        'titulo',
        'capa',
    ];
    // 'cpf_cnpj'

    public function rules() {

        return [
            'titulo' => 'required',
            'capa' => 'image' ,            
        ];
        // 'cpf_cnpj' => 'required|unique:clientes'
    }

    // vai ser usado no controller para saber o nome do campo image, pois poderia ser avatar
    // ou qualquer outro nome
    public function arquivo($id) {
        $data = $this->find($id);
        // nome que ta na tabel para armazenar a foto
        return $data->capa;
    }
}
