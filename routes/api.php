<?php


use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Route; 

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Rotas de filmes
Route::post('/login' , 'Auth\AuthenticateController@authenticate' );
Route::post('/login-refresh' , 'Auth\AuthenticateController@refresh' );
Route::get('/me' , 'Auth\AuthenticateController@getAuthenticatedUser' );

Route::group(['middleware' => ['auth:api'] , 'namespace' => 'Api\\'], function() {



Route::get('/filmes' , 'FilmeApiController@index' );
Route::get('/clientes/{id}/filmesAlugados' , 'ClienteApiController@alugados' );
Route::post('/filmes' , 'FilmeApiController@store' );


Route::get('/clientes', 'ClienteApiController@index' );
Route::post('/clientes', 'ClienteApiController@store' );
Route::get('/clientes/{id}', 'ClienteApiController@show' );
Route::delete('/clientes/{id}', 'ClienteApiController@destroy' );
Route::put('/clientes/{id}', 'ClienteApiController@update' );
// $this->apiResource('clientes', 'ClienteApiController');



// $this->get('documento/{id}/cliente' , 'DocumentoApiController@cliente' );
Route::get('/documento/{id}/cliente' , 'DocumentoApiController@cliente' );
Route::get('/clientes/{id}/documento' , 'ClienteApiController@documento' );
Route::apiResource('/documento', 'DocumentoApiController' );



Route::get('/telefone/{id}/cliente' , 'TelefoneApiController@cliente' );
Route::get('/clientes/{id}/telefone' , 'ClienteApiController@telefone' );
Route::post('/telefone' , 'TelefoneApiController@store' );
Route::get('/telefone' , 'TelefoneApiController@index' );

});


